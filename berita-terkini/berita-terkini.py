import streamlit as st
import requests

# Fungsi untuk mendapatkan top berita Indonesia dari API berdasarkan pencarian
def get_top_headlines_by_search(query):
    url = "https://newsapi.org/v2/top-headlines"
    params = {
        "country": "id",
        "apiKey": "a3cd76be4eee4043b4c744f82b3a2691",
        "q": query
    }
    response = requests.get(url, params=params)
    data = response.json()
    return data

# Mendapatkan top berita Indonesia dari API
top_headlines = get_top_headlines_by_search("")

# Menampilkan judul halaman
st.title("Top Berita Indonesia")

# Menampilkan keterangan aplikasi
st.markdown("Aplikasi ini menampilkan top berita Indonesia menggunakan News API.")

# Menambahkan kotak teks untuk pencarian berita dan tombol Submit
search_query = st.text_input("Cari Berita")
submit_button = st.button("Submit")

# Menggunakan pencarian berita jika tombol Submit ditekan
if submit_button:
    top_headlines = get_top_headlines_by_search(search_query)

# Menampilkan daftar berita
for article in top_headlines['articles']:
    st.subheader(article['title'])
    st.write("Sumber: " + article['source']['name'])
    st.write("Tanggal: " + article['publishedAt'])
    
    # Periksa jika deskripsi adalah None sebelum menggabungkan dengan string
    description = article['description']
    if description is not None:
        st.write("Deskripsi: " + description)
        
    st.write("Artikel Lengkap: [Buka Artikel](" + article['url'] + ")")
    st.write("---")
