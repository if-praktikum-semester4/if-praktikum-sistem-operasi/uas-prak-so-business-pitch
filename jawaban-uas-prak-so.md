# Web Service Sederhana
# 1. Pembuatan Docker Image
## Instalasi Dependensi atau Package Yang Dibutuhkan
Instalasi package pada Dockerfile dari web sederhana ini dapat dilakukan pada base image Debian atau Ubuntu. Karena instalasi paket menggunakan perintah apt-get, yang umumnya digunakan pada base image yang menggunakan manajer paket APT seperti Debian atau Ubuntu.

```dockerfile
RUN apt-get update && apt-get install -y git neovim netcat python3-pip
```
Perintah tersebut akan mengupdate package list terlebih dahulu, kemudian akan menginstall package **Git**, **Neovim**, **Netcat**, dan **Python 3** dengan **pip** dalam Docker image.

## Build Web Service Ke Dalam Docker Images
Proses build dilakukan dengan menentukan terlebih dahulu base images yang akan digunakan. Pada aplikasi ini base image menggunakan `debian:bullseye-slim` sebagai base image. Sehingga Docker image yang akan dibangun akan berdasarkan sistem operasi Debian versi Bullseye dalam versi yang lebih ringan (slim).

```dockerfile
FROM debian:bullseye-slim
```
Selanjutnya adalah meng-copy seluruh file projectnya, kemudian install dependensi yang dibutuhkan project, set working directory, expose port yang akan digunakan, build Dockerfile dan run.

```dockerfile
# Copy web service files
COPY berita-terkini.py /home/berita-terkini/berita-terkini.py
COPY requirements.txt home/berita-terkini/requirements.txt

# Install Python dependencies
RUN pip3 install --no-cache-dir -r /home/berita-terkini/requirements.txt

# Set working directory
WORKDIR /home/berita-terkini

# Expose port
EXPOSE 25137

# Start web service
CMD ["streamlit", "run", "--server.port=25137", "berita-terkini.py"]
```
# 2. Dockerfile

![top](SS/dockerfile.png)

Berikut merupakan link Dockerfile dan file lainnya:
- Link Dockerfile: [Dokerfile - gitlab](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/uas-prak-so-business-pitch/-/blob/main/berita-terkini/Dockerfile) 
- Link file lainnya: [file lainnya - gitlab](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/uas-prak-so-business-pitch/-/tree/main/berita-terkini)

# 3. Webservice
Webservice pada aplikasi ini merupakan NewsAPI yakni RESTful API dengan menggunakan protokol HTTP untuk berkomunikasi dan mengirimkan data dalam format JSON. Diakses dengan link API: [https://newsapi.org/v2/top-headlines](https://newsapi.org/v2/top-headlines), dengan APIKey `a3cd76be4eee4043b4c744f82b3a2691`.

Link akses aplikasi webservice yang telah dibuat: [http://135.181.26.148:25137/](http://135.181.26.148:25137/)

# 4. Docker Compose

![top](SS/docker_compose.png)

Berikut merupakan link Docker Compose dan file terkaitnya:
- Docker Compose: [Docker Compose - gitlab](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/uas-prak-so-business-pitch/-/blob/main/berita-terkini/docker-compose.yml)

- File terkaitnya: [file terkaitnya - gitlab](https://gitlab.com/if-praktikum-semester4/if-praktikum-sistem-operasi/uas-prak-so-business-pitch/-/blob/main/berita-terkini/requirements.txt)


# 5. Deskripsi Project
## Berita-Terkini / Aplikasi Top Berita Indonesia

![top](SS/berita-terkini.png)

Sebuah aplikasi web sederhana dibangun dengan menggunakan framework Streamlit untuk menciptakan antarmuka pengguna, dan menggunakan library `request` untuk melakukan permintaan HTTP ke API.

### Deskripsi Project
Aplikasi ini bertujuan untuk memberikan informasi terkait berita-berita dari berbagai macam portal berita terkini yang terdapat di Indonesia. Informasi berita-berita akan tampil secara real-time dengan terus update. Pengguna dapat mencari data berita yang sedang ditampilkan. Penguna juga dapat mengakses halaman berita tersebut pada sumber penerbit berita. Aplikasi akan mengambil data-data informasi berita dari berbagai sumber dari API News API dengan penggunaan permintaan HTTP. 

### Penggunaan Sistem Operasi Dalam Containerization
Containerisasi merupakan suatu teknik untuk mengemas aplikasi dan file terkait ke dalam suatu package yang disebut container. Dengan menggunakan container, suatu aplikasi dapat dikirim dan dijalankan dengan mudah di lingkungan yang terisolasi, tanpa khawatir tentang perbedaan konfigurasi dan dependensi di lingkungan tujuan. Artinya, kontainer dapat memungkinkan pengemasan aplikasi dalam unit yang portabel, praktis, serta mudah untuk dijalankan di berbagai sistem. Hal ini karena sifat dari container tersebut yakni terisolasi, sehingga container dapat berjalan di lingkungan yang terpisah tanpa saling mengganggu satu sama lain.

Sistem operasi digunakan dalam proses containerization yakni sebagai dasar untuk menjalankan container, menyediakan lingkungan terisolasi, mengatur sumber daya, memfasilitasi jaringan, menjaga keamanan, dan menyimpan data. Sehingga memungkinkan container untuk berjalan secara portabel, efisien, dan konsisten di berbagai lingkungan dan infrastruktur yang berbeda.

### Peran Containerization Dapat Membantu Mempermudah Pengembangan Aplikasi
Container berisi semua yang dibutuhkan untuk menjalankan aplikasi, termasuk kode, runtime, sistem operasi, dan pustaka yang relevan. Sehingga container memberikan manfaat dalam pengembangan aplikasi dengan menyediakan isolasi lingkungan, kemudahan dalam mengelola dependensi, skalabilitas yang jelas, dan kemampuan untuk menghasilkan dan menguji aplikasi secara konsisten. Containerisasi juga memungkinkan developer untuk mengisolasi aplikasi dari lingkungan host, sehingga aplikasi dapat berjalan seragam di berbagai lingkungan.

containerisasi dapat membantu mempermudah pengembangan aplikasi dengan beberapa cara seperti:
- Portabilitas. Dimana container menyediakan portabilitas yang tinggi karena berisi semua dependensi yang dibutuhkan untuk menjalankan aplikasi.
- Konsistensi. Dimana containerisasi pengembang dapat menyusun semua dependensi aplikasi ke dalam container serta memastikan bahwa aplikasi berjalan dengan cara yang konsisten di seluruh lingkungan.
- Skalabilitas. Container juga dapat memungkinkan pengembang untuk dengan mudah mengelola replikasi aplikasi.
- Pengelolaan dependensi. Container juga memungkinkan pengembang untuk mengisolasi dependensi aplikasi, termasuk versi library dan tools ke dalam container sendiri.
- Kolaborasi tim. Dengan menggunakan containerjuga, pengembang dapat memastikan bahwa semua anggota tim menggunakan lingkungan yang sama untuk mengembangkan dan menjalankan aplikasi.
- Uji dan penerapan yang lebih cepat. Containerisasi memungkinkan pengembang juga untuk dengan cepat membuat salinan aplikasi dalam kondisi tertentu untuk pengujian dan penerapan. 

Dari hal tersebut sehingga memungkinkan pengembang untuk lebih fokus pada logika bisnis aplikasi mereka daripada menghabiskan waktu berurusan dengan perbedaan lingkungan dan konfigurasi.

### Apa itu DevOps Dan Peran DevOps dalam membantu proses pengembangan Aplikasi
DevOps adalah prinsip untuk mengotomatiskan dan mengintegrasikan proses antara pengembangan software (Dev) dan kegiatan operasional (Ops) sehingga membuat pengembangan dan operasi software menjadi lebih mudah dengan menggunakan otomatisasi dan kerjasama tim. Tujuannya adalah agar software yang dibuat lebih baik dan cepat. DevOps melibatkan penggunaan alat dan praktik untuk membantu tim bekerja bersama-sama. Tugas utama DevOps adalah membuat, memperbaiki, dan memeriksa software.

DevOps membantu pengembangan aplikasi dengan cara mempercepat proses pengembangan dan pengiriman, meningkatkan kolaborasi tim, menerapkan otomatisasi, menggunakan praktik Continuous Integration dan Continuous Deployment (CI/CD) untuk memastikan pengiriman aplikasi yang cepat dan berkualitas tinggi, memantau kinerja aplikasi secara terus-menerus, memperhatikan skalabilitas dan resiliensi, dan memantau proses pengembangan untuk meningkatkan efisiensi dan keberhasilan pengembangan. Dengan demikian, DevOps membantu tim pengembangan mengurangi waktu pengembangan, meningkatkan kualitas aplikasi, dan memberikan nilai bisnis yang lebih cepat kepada pengguna akhir.

### Contoh Kasus Penerapan DevOps Dalam Perusahaan
Salah satu contoh kasus dari penerapan DevOps dalam perusahaan adalah pada Twitter. Twitter yang merupakan suatu platform media sosial yang populer dengan jutaan pengguna aktif, menerapkan DevOps untuk memastikan pengiriman fitur yang cepat dan menjaga tingkat ketersediaan tinggi. Dimana Twitter membentuk tim DevOps yang terdiri dari pengembang, pengelola infrastruktur, dan profesional operasi, yang bekerja secara kolaboratif untuk mengoordinasikan pengembangan, pengujian, dan penerapan aplikasi. 

Cara yang dilakukannya adalah dengan menerapkan praktik Continuous Integration (CI) dan Continuous Deployment (CD), serta dengan melakukan otomatisasi untuk mengintegrasikan perubahan kode dengan cepat dan menerapkannya ke produksi secara otomatis. Twitter juga menggunakan infrastruktur pemantauan yang canggih untuk melacak kinerja aplikasi secara real-time, serta otomatisasi infrastruktur dan pengujian yang membantu meningkatkan efisiensi dan kehandalan. Sehingga DevOps telah membantu Twitter dalam menghadapi skala pengguna yang tinggi dan memberikan pengalaman pengguna yang responsif dengan mengoptimalkan proses pengembangan dan pengiriman aplikasi mereka.

# 6. Demonstrasi Web Sederhana

Berikut link video demonstrasi [web sederhana](https://youtu.be/ggrCxxY9r_k)


# 7. Docker Hub

![top](SS/docker_hub.png)

Berikut link docker hub: [Docker Hub](https://hub.docker.com/repository/docker/sumitradrian/berita-terkini/)




